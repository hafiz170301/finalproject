﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace FinalProjectAPI.Models;

public partial class FinalProjectContext : DbContext
{
    public FinalProjectContext()
    {
    }

    public FinalProjectContext(DbContextOptions<FinalProjectContext> options)
        : base(options)
    {
    }

    public virtual DbSet<TabelBarang> TabelBarangs { get; set; }

    public virtual DbSet<TabelKategori> TabelKategoris { get; set; }

    public virtual DbSet<TabelUser> TabelUsers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=FinalProject;Trusted_Connection=True;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TabelBarang>(entity =>
        {
            entity.HasKey(e => e.IdBarang);

            entity.ToTable("TabelBarang");

            entity.Property(e => e.IdBarang).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Deskripsi)
                .HasMaxLength(255)
                .IsUnicode(false);
            entity.Property(e => e.IdKategori).HasDefaultValueSql("(newid())");
            entity.Property(e => e.NamaBarang)
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.HasOne(d => d.IdKategoriNavigation).WithMany(p => p.TabelBarangs)
                .HasForeignKey(d => d.IdKategori)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TabelBarang_TabelKategori");
        });

        modelBuilder.Entity<TabelKategori>(entity =>
        {
            entity.HasKey(e => e.IdKategori);

            entity.ToTable("TabelKategori");

            entity.Property(e => e.IdKategori).HasDefaultValueSql("(newid())");
            entity.Property(e => e.NamaKategori)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TabelUser>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("TabelUser");

            entity.Property(e => e.IdUser).ValueGeneratedOnAdd();
            entity.Property(e => e.Password)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Username)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
