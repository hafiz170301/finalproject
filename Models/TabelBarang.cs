﻿using System;
using System.Collections.Generic;

namespace FinalProjectAPI.Models;

public partial class TabelBarang
{
    public Guid IdBarang { get; set; }

    public string NamaBarang { get; set; } = null!;

    public int Stock { get; set; }

    public int Harga { get; set; }

    public Guid IdKategori { get; set; }

    public string? Deskripsi { get; set; }

    public virtual TabelKategori? IdKategoriNavigation { get; set; }
}
