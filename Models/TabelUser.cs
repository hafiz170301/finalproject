﻿using System;
using System.Collections.Generic;

namespace FinalProjectAPI.Models;

public partial class TabelUser
{
    public int IdUser { get; set; }

    public string Username { get; set; } = null!;

    public string Password { get; set; } = null!;
}
