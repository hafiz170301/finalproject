﻿using System;
using System.Collections.Generic;

namespace FinalProjectAPI.Models;

public partial class TabelKategori
{
    public Guid IdKategori { get; set; }

    public string NamaKategori { get; set; } = null!;

    public virtual ICollection<TabelBarang> TabelBarangs { get; set; } = new List<TabelBarang>();
}
