﻿using FinalProjectAPI.Models;
using FinalProjectAPI.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FinalProjectAPI.Controllers.ProductsController;

[Route("api/[controller]")]
[ApiController]
public class ProductsController : ControllerBase
{
    private readonly IProduct _product;

    public ProductsController(IProduct _product)
    {
        this._product = _product;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllProducts()
    {
        List<TabelBarang> products = await _product.GetAllProducts();

        return Ok(products);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetProduct(Guid id) 
    {
        TabelBarang product = await _product.GetProduct(id);

        return Ok(product);
    }

    [HttpPost]
    public IActionResult CreateProduct(TabelBarang product)
    {
        _product.CreateProduct(product);

        return Created("CreatedProduct", product);
    }

    [HttpPut("{id}")]
    public IActionResult UpdateProduct(Guid id, TabelBarang product)
    {
        _product.UpdateProduct(id, product);

        return Ok("product has been updated");
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteProduct(Guid id)
    {
        _product.DeleteProduct(id);

        return Ok("product has been deleted");
    }
}
