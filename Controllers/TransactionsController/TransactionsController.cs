﻿using FinalProjectAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinalProjectAPI.Controllers.TransactionsController
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        readonly FinalProjectContext _dbContext;

        public TransactionsController(FinalProjectContext _dbContext)
        {
            this._dbContext = _dbContext;
        }

        [HttpPost]
        public async Task<IActionResult> CreateTransaction(Guid id, int total)
        {
            TabelBarang product = await GetProduct(id);

            if (product == null)
                return TransactionResponse("404");
            if (!ValidateProduct(product.Stock, total))
                return TransactionResponse("400");

            product.Stock = product.Stock - total;

            _dbContext.Update(product);
            _dbContext.SaveChanges();

            return TransactionResponse();
        }

        private async Task<TabelBarang> GetProduct(Guid id)
        {
            return await _dbContext.TabelBarangs.Where(val => val.IdBarang == id).FirstOrDefaultAsync();
        }

        private bool ValidateProduct(int currentStock, int total)
        {
            return currentStock >= total;
        }

        private IActionResult TransactionResponse(string status = "200")
        {
            if (status == "404")
                return NotFound("product not found");

            if (status == "400")
                return BadRequest("insufficient stock");

            return Ok("transaction has been created");
        }
    }
}
