﻿using FinalProjectAPI.Models;
using FinalProjectAPI.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace FinalProjectAPI.Controllers.CategoriesController
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategory _category;

        public CategoriesController(ICategory _category) 
        {
            this._category = _category;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCategories()
        {
            List<TabelKategori> categories = await _category.GetAllCategories();

            return Ok(categories);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategory(Guid id)
        {
            TabelKategori category = await _category.GetCategory(id);

            return Ok(category);
        }

        [HttpPost]
        public IActionResult CreateCategory(TabelKategori category)
        {
            _category.CreateCategory(category);

            return Created("CreateCategory", category);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCategory(Guid id, TabelKategori category)
        {
            _category.UpdateCategory(id, category);

            return Ok("category has been updated");
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCategory(Guid id)
        {
            _category.DeleteCategory(id);

            return Ok("category has been deleted");
        }
    }
}
