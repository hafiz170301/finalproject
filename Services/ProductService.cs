﻿using FinalProjectAPI.Models;
using FinalProjectAPI.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FinalProjectAPI.Services;

public class ProductService : IProduct
{
    readonly FinalProjectContext _dbContext;

    public ProductService(FinalProjectContext _dbContext)
    {
        this._dbContext = _dbContext;
    }

    public async Task<List<TabelBarang>> GetAllProducts()
    {
        List<TabelBarang> products = await _dbContext.TabelBarangs
            .Join(_dbContext.TabelKategoris, product => product.IdKategori, category => category.IdKategori,
            (product, category) => new TabelBarang
            {
                IdBarang = product.IdBarang,
                IdKategori = product.IdKategori,
                NamaBarang = product.NamaBarang,
                Deskripsi = product.Deskripsi,
                Harga = product.Harga,
                Stock = product.Stock,
                IdKategoriNavigation = category
            }).ToListAsync();

        return products;
    }

    public async Task<TabelBarang> GetProduct(Guid id)
    {
        TabelBarang product = await _dbContext.TabelBarangs.Where(val => val.IdBarang == id).FirstAsync();

        return product;
    }

    public void CreateProduct(TabelBarang product)
    {
        _dbContext.Add(product);
        _dbContext.SaveChanges();
    }

    public void UpdateProduct(Guid id, TabelBarang product)
    {
        TabelBarang currentProduct = _dbContext.TabelBarangs.Where(val => val.IdBarang == id).First();

        currentProduct.IdKategori = product.IdKategori;
        currentProduct.NamaBarang = product.NamaBarang;
        currentProduct.Deskripsi = product.Deskripsi;
        currentProduct.Harga = product.Harga;
        currentProduct.Stock = product.Stock;

        _dbContext.Update(currentProduct);
        _dbContext.SaveChanges();
    }

    public void DeleteProduct(Guid id)
    {
        TabelBarang product = _dbContext.TabelBarangs.Where(val => val.IdBarang == id).First();

        _dbContext.Remove(product);
        _dbContext.SaveChanges();
    }
}
