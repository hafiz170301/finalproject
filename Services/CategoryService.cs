﻿using FinalProjectAPI.Models;
using FinalProjectAPI.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FinalProjectAPI.Services;

public class CategoryService : ICategory
{
    readonly FinalProjectContext _dbContext;

    public CategoryService(FinalProjectContext _dbContext)
    {
        this._dbContext = _dbContext;
    }

    public async Task<List<TabelKategori>> GetAllCategories()
    {
        List<TabelKategori> categories = await _dbContext.TabelKategoris.ToListAsync();

        return categories;
    }

    public async Task<TabelKategori> GetCategory(Guid id)
    {
        TabelKategori category = await _dbContext.TabelKategoris.Where(val => val.IdKategori == id).FirstAsync();

        return category;
    }

    public void CreateCategory(TabelKategori category)
    {
        _dbContext.Add(category);
        _dbContext.SaveChanges();
    }

    public void UpdateCategory(Guid id, TabelKategori category)
    {
        TabelKategori currentCategory = _dbContext.TabelKategoris.Where(val => val.IdKategori == id).First();

        currentCategory.NamaKategori = category.NamaKategori;

        _dbContext.Update(currentCategory);
        _dbContext.SaveChanges();
    }

    public void DeleteCategory(Guid id)
    {
        TabelKategori category = _dbContext.TabelKategoris.Where(val => val.IdKategori == id).First();

        _dbContext.Remove(category);
        _dbContext.SaveChanges();
    }
}
