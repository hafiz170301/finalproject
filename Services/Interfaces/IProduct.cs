﻿using FinalProjectAPI.Models;

namespace FinalProjectAPI.Services.Interfaces;

public interface IProduct
{
    Task<List<TabelBarang>> GetAllProducts();
    Task<TabelBarang> GetProduct(Guid id);
    void CreateProduct(TabelBarang product);
    void UpdateProduct(Guid id, TabelBarang product);
    void DeleteProduct(Guid id);

}
