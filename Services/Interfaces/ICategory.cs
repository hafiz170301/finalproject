﻿using FinalProjectAPI.Models;

namespace FinalProjectAPI.Services.Interfaces;

public interface ICategory
{
    Task<List<TabelKategori>> GetAllCategories();
    Task<TabelKategori> GetCategory(Guid id);
    void CreateCategory(TabelKategori category);
    void UpdateCategory(Guid id, TabelKategori category);
    void DeleteCategory(Guid id);
}
